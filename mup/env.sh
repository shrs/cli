# Default settings for Meteor-up deployment
# To override, create an env.sh in config/env
# e.g. config/production/env.sh
export MUP_HOST="www.shoulders.rocks"
export MUP_HOST_USER="ubuntu"
export MUP_PRIVATE_KEY=".private_key"
export MUP_SETUP_MONGO="true"
export MUP_SETUP_NODE="true"
export MUP_SETUP_PHANTOM="true"
export MUP_NODE_VERSION="0.10.40"
export MUP_APP_NAME="shoulders"
export MUP_ENV_PORT="80"
export MUP_APP_DIR=$PWD;
